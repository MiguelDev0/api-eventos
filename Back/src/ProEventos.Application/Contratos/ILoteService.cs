using System.Threading.Tasks;
using ProEventos.Application.Dtos;

namespace ProEventos.Application.Contratos
{
    public interface ILoteService
    {
        Task<LoteDto> SaveLotes(int eventoId, EventoDto[] model);
        Task<bool> DeleteEvento(int eventoId, int loteId);
        Task<EventoDto[]> GetLotesByEventoIdAsync(int eventoId);
        Task<EventoDto[]> GetLotesByIdsAsync(int eventoId, int loteId);        
    }
}