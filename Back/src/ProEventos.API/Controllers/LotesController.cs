﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using ProEventos.Persistence;
using ProEventos.Domain;
using ProEventos.Persistence.Context;
using ProEventos.Application.Contratos;
using Microsoft.AspNetCore.Http;
using System;
using System.Threading.Tasks;
using ProEventos.Application.Dtos;

namespace ProEventos.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]  

    public class LotesController : ControllerBase
    {        
        private readonly ILoteService _loteService;
        public LotesController(ILoteService LoteService)
        {            
            _loteService = LoteService;
        }

        [HttpGet("{eventoId}")]
        public async Task<IActionResult> Get()
        {   
            try
            {
                var eventos = await _eventosService.GetEventoByIdAsync(true);
                if(eventos == null) return NoContent();
                
                return Ok(eventos);
            }
            catch (Exception ex)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, $"Erro ao tentar recuperar eventos. Erro: {ex.Message}");                
            }
        }

        [HttpPut("{eventoId}")]
        public async Task<IActionResult> Put(int eventoid, LoteDto[] models)
        {
            try
            {
                var eventos = await _eventosService.UpdateEvento(id, models);
                if(eventos == null) return NoContent();

                return Ok(eventos);
            }
            catch (Exception ex)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError,
                    $"Erro ao tentar atualizar eventos. Erro: {ex.Message}");                
            }
        }

        [HttpDelete("{eventoId}/{loteId}")]
        public async Task<IActionResult> Delete(int eventoId, int loteId)
        {
            try
            {
                var evento = await _eventosService.GetEventoByIdAsync(id, true);
                if(evento == null) return NoContent();

                return await _eventosService.DeleteEvento(id) 
                    ? Ok(new { message = "Deletado" }) 
                    : throw new Exception("Ocorreu um problema não específico ao tentar deletar Evento.");
            }
            catch (Exception ex)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, $"Erro ao tentar deletar eventos. Erro: {ex.Message}");
            }
        }
    }

}
